import * as THREE from 'https://unpkg.com/three@0.123.0/build/three.module.js';

import Stats from 'https://unpkg.com/three@0.123.0/examples/jsm/libs/stats.module.js';

import { PLYLoader } from 'https://unpkg.com/three@0.123.0/examples/jsm/loaders/PLYLoader.js';


const cameraX = 60;
const cameraHeight = 20;

const cameraTargetX = 40;

let container, stats;

let camera, cameraTarget, scene, renderer;

init();
animate();

function init() {

    container = document.createElement( 'div' );
    document.body.appendChild( container );

    camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 1, 150 );
    camera.position.set( -cameraX, cameraHeight, 0 );

    cameraTarget = new THREE.Vector3( cameraTargetX, 0, 0 );

    scene = new THREE.Scene();
    scene.background = new THREE.Color( 0x72645b );

    // Ground

    const plane = new THREE.Mesh(
        new THREE.PlaneBufferGeometry( 80, 80 ),
        new THREE.MeshPhongMaterial( { color: 0x999999, specular: 0x101010 } )
    );
    plane.rotation.x = - Math.PI / 2;
    plane.position.x = 40
    plane.position.y = 0;
    plane.position.z = 0
    scene.add( plane );

    plane.receiveShadow = true;


    // PLY file

    const loader = new PLYLoader();


    loader.load( './models/ply/binary/mesh_11_v9.ply', function ( geometry ) {

        geometry.computeVertexNormals();

        const material = new THREE.MeshStandardMaterial( { color: 0x0055ff, flatShading: true } );
        const mesh = new THREE.Mesh( geometry, material );

        mesh.position.x = - 0.0;
        mesh.position.y = - 0.0;
        mesh.position.z = - 0.0;
        mesh.rotation.x = - Math.PI / 2;
        mesh.scale.multiplyScalar( 1 );

        mesh.castShadow = true;
        mesh.receiveShadow = true;

        scene.add( mesh );

    } );

    // Path

    loadPath('./paths/lh rear fuselage - section 46.json');

    // Lights

    scene.add( new THREE.HemisphereLight( 0x443333, 0x111122 ) );

    addShadowedLight( 100, 100, 1, 0xffffff, 1.35 );
    // addShadowedLight( 0.5, 1, - 1, 0xffaa00, 1 );

    // renderer

    renderer = new THREE.WebGLRenderer( { antialias: true } );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight );
    renderer.outputEncoding = THREE.sRGBEncoding;

    renderer.shadowMap.enabled = true;

    container.appendChild( renderer.domElement );

    // stats

    stats = new Stats();
    container.appendChild( stats.dom );

    // resize

    window.addEventListener( 'resize', onWindowResize, false );

}

function loadPath(filename) {
    //create a blue LineBasicMaterial
    const material = new THREE.LineBasicMaterial( { color: 0xFF0000 } );

    fetch(filename)
        .then(response=>response.json())
        .then(flightPath=>{
            const pLength = flightPath.length;
            const points = [];
            for (const point of flightPath) {
                points.push(
                    new THREE.Vector3(
                        point.current_waypoint.translation.x,
                        point.current_waypoint.translation.z,
                        point.current_waypoint.translation.y,
                    )
                )
            }
            const geometry = new THREE.BufferGeometry().setFromPoints( points );
            const line = new THREE.Line( geometry, material );
            line.rotation.x - Math.PI / 2;
            scene.add( line );
    });
}

function addShadowedLight( x, y, z, color, intensity ) {

    const directionalLight = new THREE.DirectionalLight( color, intensity );
    directionalLight.position.set( x, y, z );
    scene.add( directionalLight );

    directionalLight.castShadow = true;

    const d = 1;
    directionalLight.shadow.camera.left = - d;
    directionalLight.shadow.camera.right = d;
    directionalLight.shadow.camera.top = d;
    directionalLight.shadow.camera.bottom = - d;

    directionalLight.shadow.camera.near = 1;
    directionalLight.shadow.camera.far = 4;

    directionalLight.shadow.mapSize.width = 1024;
    directionalLight.shadow.mapSize.height = 1024;

    directionalLight.shadow.bias = - 0.001;

}

function onWindowResize() {

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize( window.innerWidth, window.innerHeight );

}

function animate() {

    requestAnimationFrame( animate );

    render();
    stats.update();

}

function render() {

    const timer = Date.now() * 0.0005;

    camera.position.x = Math.sin( timer ) * cameraX + cameraTargetX;
    camera.position.z = Math.cos( timer ) * cameraX;

    camera.lookAt( cameraTarget );

    renderer.render( scene, camera );

}
